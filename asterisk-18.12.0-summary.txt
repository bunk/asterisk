                                Release Summary

                                asterisk-18.12.0

                                Date: 2022-05-12

                           <asteriskteam@digium.com>

     ----------------------------------------------------------------------

                               Table of Contents

    1. Summary
    2. Contributors
    3. Closed Issues
    4. Other Changes
    5. Diffstat

     ----------------------------------------------------------------------

                                    Summary

                                 [Back to Top]

   This release is a point release of an existing major version. The changes
   included were made to address problems that have been identified in this
   release series, or are minor, backwards compatible new features or
   improvements. Users should be able to safely upgrade to this version if
   this release series is already in use. Users considering upgrading from a
   previous version are strongly encouraged to review the UPGRADE.txt
   document as well as the CHANGES document for information about upgrading
   to this release series.

   The data in this summary reflects changes that have been made since the
   previous release, asterisk-18.11.0.

     ----------------------------------------------------------------------

                                  Contributors

                                 [Back to Top]

   This table lists the people who have submitted code, those that have
   tested patches, as well as those that reported issues on the issue tracker
   that were resolved in this release. For coders, the number is how many of
   their patches (of any size) were committed into this release. For testers,
   the number is the number of times their name was listed as assisting with
   testing a patch. Finally, for reporters, the number is the number of
   issues that they reported that were affected by commits that went into
   this release.

   Coders                              Testers    Reporters                   
   22 Naveen Albert                               18 N A                      
   6 Joshua C. Colp                               3 Mark Petersen             
   5 Sean Bright                                  2 Rusty Newton              
   4 Asterisk Development Team                    2 Michael Auracher          
   4 Ben Ford                                     2 George Joseph             
   3 Mark Petersen                                2 Michael Auracher          
   2 Kevin Harwell                                1 Michael Cargile           
   2 Philip Prindeville                           1 Andre Heider              
   2 George Joseph                                1 Alexei Gradinari          
   2 Maximilian Fridrich                          1 Daniel Bonazzi            
   1 Birger Harzenetter (license 5870)            1 Claude Diderich           
   1 Alexei Gradinari                             1 Scott Griepentrog         
   1 Michael Cargile                              1 Arix                      
   1 Hugh McMaster                                1 Stefan Ruijsenaars        
   1 Boris P. Korzun                              1 Benjamin Keith Ford       
   1 Yury Kirsanov                                1 LA                        
   1 Marcel Wagner                                1 Josh Hogan                
                                                  1 Ross Beer                 
                                                  1 Boris P. Korzun           
                                                  1 Marcel Wagner             
                                                  1 INVADE International Ltd. 
                                                  1 Leandro Dardini           
                                                  1 Yury Kirsanov             
                                                  1 Clint Ruoho               
                                                  1 Jim Van Meggelen          
                                                  1 Stefan Ruijsenaars        
                                                  1 Tzafrir Cohen             
                                                  1 Hugh McMaster             
                                                  1 Jonathan Harris           
                                                  1 David Herselman           
                                                  1 Dmitriy Serov             
                                                  1 Philip Prindeville        
                                                  1 Gregory Massel            
                                                  1 Jasper Hafkenscheid       
                                                  1 Kevin Harwell             
                                                  1 Josh Alberts              
                                                  1 Sebastian Gutierrez       

     ----------------------------------------------------------------------

                                 Closed Issues

                                 [Back to Top]

   This is a list of all issues from the issue tracker that were closed by
   changes that went into this release.

  Security

    Category: Functions/func_odbc

   ASTERISK-29838: ${SQL_ESC()} not correctly escaping a terminating \
   Reported by: Leandro Dardini
     * [39cd09c246] Joshua C. Colp -- func_odbc: Add SQL_ESC_BACKSLASHES
       dialplan function.

    Category: Resources/res_stir_shaken

   ASTERISK-29476: res_stir_shaken: Blind SSRF vulnerabilities
   Reported by: Clint Ruoho
     * [11accf8064] Ben Ford -- AST-2022-002 - res_stir_shaken/curl: Add ACL
       checks for Identity header.
   ASTERISK-29872: res_stir_shaken: Resource exhaustion with large files
   Reported by: Benjamin Keith Ford
     * [33091c2659] Ben Ford -- AST-2022-001 - res_stir_shaken/curl: Limit
       file size and check start.

  New Feature

    Category: Applications/app_confbridge

   ASTERISK-29931: Option to allow a user to not hear the join sound on enter
   but everyone else can
   Reported by: Michael Cargile
     * [216a55408e] Michael Cargile -- apps/confbridge: Added
       hear_own_join_sound option to control who hears sound_join

    Category: Applications/app_queue

   ASTERISK-29876: app_queue: Add music on hold option
   Reported by: N A
     * [b7edc08e33] Naveen Albert -- app_queue: Add music on hold option to
       Queue.

    Category: Channels/chan_pjsip

   ASTERISK-29941: chan_pjsip: Add ability to send flash events
   Reported by: N A
     * [8bc6d42a27] Naveen Albert -- chan_pjsip: Add ability to send flash
       events.

    Category: Functions/General

   ASTERISK-29820: cli: Add command to evaluate a function
   Reported by: N A
     * [3686a97d79] Naveen Albert -- cli: Add command to evaluate dialplan
       functions.

    Category: Functions/NewFeature

   ASTERISK-29486: Hint-like extension value lookup function without device
   state
   Reported by: N A
     * [79689d9df8] Naveen Albert -- func_evalexten: Extension evaluation
       function.

    Category: Functions/func_db

   ASTERISK-29968: func_db: Add a function to return cardinality of keys at
   prefix
   Reported by: N A
     * [ce00f8758d] Naveen Albert -- func_db: Add function to return
       cardinality at prefix

  Bug

    Category: Applications/app_meetme

   ASTERISK-30002: app_meetme: Don't erroneously set global variables when
   channel is NULL
   Reported by: N A
     * [ff70b2aac6] Naveen Albert -- app_meetme: Don't erroneously set global
       variables.

    Category: Bridges/bridge_simple

   ASTERISK-29253: Incorrect bridging on transfer
   Reported by: Yury Kirsanov
     * [6ac08fdcf8] Yury Kirsanov -- bridge_simple.c: Unhold channels on join
       simple bridge.

    Category: CDR/cdr_adaptive_odbc

   ASTERISK-30023: cdr_adaptive_odbc: does not support DATETIME database
   columns
   Reported by: Gregory Massel
     * [ec8ab44b7f] Joshua C. Colp -- cdr_adaptive_odbc: Add support for
       SQL_DATETIME field type.

    Category: Channels/chan_dahdi

   ASTERISK-28518: chan_dahdi: Caller ID FSK Erroneously Sent when Picking Up
   Dahdi Call On Hold
   Reported by: Josh Alberts
     * [a4f04666b5] Naveen Albert -- chan_dahdi: Don't allow MWI FSK if
       channel not idle.
   ASTERISK-29990: chan_dahdi: adding ring cadences is not idempotent on
   dahdi restart
   Reported by: N A
     * [cb53ad5671] Naveen Albert -- chan_dahdi: Don't append cadences on
       dahdi restart.
   ASTERISK-29994: chan_dahdi: Round robin array size is too small for max
   number of groups
   Reported by: N A
     * [dd15cd049f] Naveen Albert -- chan_dahdi: Fix insufficient array size
       for round robin.

    Category: Channels/chan_iax2

   ASTERISK-30007: chan_iax2: Prevent crashes due to attempted encryption
   with missing secrets
   Reported by: N A
     * [9dc321cbcb] Naveen Albert -- chan_iax2: Prevent crash if dialing
       RSA-only call without outkey.
   ASTERISK-29895: chan_iax2: Fix misaligned spacing in iax2 show netstats
   printout
   Reported by: N A
     * [d9e55250dd] Birger Harzenetter -- chan_iax2: Fix spacing in netstats
       command
   ASTERISK-29048: chan_iax2: "iax2 show registry" shows host for perceived
   Reported by: David Herselman
     * [97c499ee34] Naveen Albert -- chan_iax2: Fix perceived showing host
       address.

    Category: Channels/chan_pjsip

   ASTERISK-29842: Do not change 180 Ringing to 183 Progress even if
   early_media already enabled
   Reported by: Mark Petersen
     * [16e59db514] Mark Petersen -- chan_pjsip: add
       allow_sending_180_after_183 option
   ASTERISK-30006: res_pjsip: UDP transport does not work when
   async_operations is greater than 1
   Reported by: Ross Beer
     * [09e8667fa5] Joshua C. Colp -- res_pjsip: Always set async_operations
       to 1.

    Category: Channels/chan_sip/General

   ASTERISK-29843: Session timers get removed on UPDATE
   Reported by: Mark Petersen
     * [bb2102a991] Mark Petersen -- chan_sip.c Session timers get removed on
       UPDATE
   ASTERISK-29955: chan_sip: SIP route header is missing on UPDATE
   Reported by: Mark Petersen
     * [4f7e3d1609] Mark Petersen -- chan_sip: SIP route header is missing on
       UPDATE

    Category: Channels/chan_sip/Transfers

   ASTERISK-29955: chan_sip: SIP route header is missing on UPDATE
   Reported by: Mark Petersen
     * [4f7e3d1609] Mark Petersen -- chan_sip: SIP route header is missing on
       UPDATE

    Category: Channels/chan_vpb

   ASTERISK-30021: ast_variable_list_replace_variable uses variable with new
   keyword
   Reported by: Jasper Hafkenscheid
     * [2587e58e05] Sean Bright -- config.h: Don't use C++ keywords as
       argument names.

    Category: Core/BuildSystem

   ASTERISK-29986: build: Asterisk 18.11.0 doesn't compile when wget isn't
   available
   Reported by: Stefan Ruijsenaars
     * [dd704bbba5] George Joseph -- make_xml_documentation: Remove usage of
       get_sourceable_makeopts
   ASTERISK-29988: REGRESSION: The build process is requiring xmllint or
   xmlstarlet ro be installed when it shouldn't
   Reported by: George Joseph
     * [2d3297d4f3] George Joseph -- Makefile: Disable XML doc validation

    Category: Core/FileFormatInterface

   ASTERISK-29943: file.c: seeking to negative file offset is not prevented
   Reported by: N A
     * [ea02bc3685] Naveen Albert -- file.c: Prevent formats from seeking
       negative offsets.

    Category: Core/General

   ASTERISK-29948: iostream: Infinite TCP timeout writing data
   Reported by: N A
     * [ae1373d12d] Joshua C. Colp -- manager: Terminate session on write
       error.
   ASTERISK-29674: Adjust for 64bit time_t
   Reported by: Andre Heider
     * [f50e793665] Philip Prindeville -- time: add support for time64 libcs

    Category: Core/Logging

   ASTERISK-22246: Asterisk's "T" flag is ignored when used with "r" or "R"
   flags. (documentation bug)
   Reported by: Rusty Newton
     * [09e989f972] Naveen Albert -- asterisk.c: Warn of incompatibilities
       with remote console.
   ASTERISK-29928: logging messages truncated when using MUSL runtime
   Reported by: Philip Prindeville
     * [140c19c206] Philip Prindeville -- logger: workaround woefully small
       BUFSIZ in MUSL

    Category: Core/Netsock

   ASTERISK-29948: iostream: Infinite TCP timeout writing data
   Reported by: N A
     * [ae1373d12d] Joshua C. Colp -- manager: Terminate session on write
       error.

    Category: Core/PBX

   ASTERISK-26719: pbx: Only up to 127 includes in a dialplan context
   (AST_PBX_MAX_STACK - 1)
   Reported by: Tzafrir Cohen
     * [bd69639a6b] Naveen Albert -- pbx.c: Warn if there are too many
       includes in a context.

    Category: Documentation

   ASTERISK-29939: agi: Fix xmldoc bug with set music
   Reported by: N A
     * [dc129b6951] Naveen Albert -- res_agi: Fix xmldocs bug with set music.
   ASTERISK-28891: documentation: AGICommand_set+music documentation
   arguments displayed incorreclty
   Reported by: Jonathan Harris
     * [dc129b6951] Naveen Albert -- res_agi: Fix xmldocs bug with set music.

    Category: General

   ASTERISK-29728: menuselect: Disabled by default modules that are enabled
   are always recompiled
   Reported by: N A
     * [b4c17c2044] Naveen Albert -- menuselect: Don't erroneously recompile
       modules.
   ASTERISK-22246: Asterisk's "T" flag is ignored when used with "r" or "R"
   flags. (documentation bug)
   Reported by: Rusty Newton
     * [09e989f972] Naveen Albert -- asterisk.c: Warn of incompatibilities
       with remote console.
   ASTERISK-26582: Asterisk seems to ignore the "n" parameter for "disable
   console colorization"
   Reported by: Sebastian Gutierrez
     * [09e989f972] Naveen Albert -- asterisk.c: Warn of incompatibilities
       with remote console.

    Category: PBX/General

   ASTERISK-29950: SayNumber can handle '01' to '07', but not '08' or '09'
   Reported by: Jim Van Meggelen
     * [81a990b8d2] Sean Bright -- conversions.c: Specify that we only want
       to parse decimal numbers.

    Category: Resources/res_ari_recordings

   ASTERISK-29960: ari: Retrieving stored recording can returns wrong file
   Reported by: Arix
     * [3a7d83087b] Sean Bright -- stasis_recording: Perform a complete match
       on requested filename.

    Category: Resources/res_pjsip_nat

   ASTERISK-29411: Crash in pjsip_msg_find_hdr_by_name
   Reported by: LA
     * [ec5b449bcf] Kevin Harwell -- res_pjsip_header_funcs: wrong pool used
       tdata headers

    Category: Resources/res_pjsip_pubsub

   ASTERISK-29961: RLS: domain part of 'uri' list attribute mismatch with
   SUBSCRIBE request
   Reported by: Alexei Gradinari
     * [96a3ff9edd] Alexei Gradinari -- res_pjsip_pubsub: RLS 'uri' list
       attribute mismatch with SUBSCRIBE request

    Category: Resources/res_pjsip_sdp_rtp

   ASTERISK-26689: res_pjsip_sdp_rtp: 183 Session in Progress. Disconnecting
   channel for lack of RTP activity
   Reported by: Dmitriy Serov
     * [82dbfe7783] Boris P. Korzun -- res_pjsip_sdp_rtp: Improve detecting
       of lack of RTP activity
   ASTERISK-29929: res_pjsip_sdp_rtp: Disconnecting channel for lack of RTP
   activity in one way sessions
   Reported by: Boris P. Korzun
     * [82dbfe7783] Boris P. Korzun -- res_pjsip_sdp_rtp: Improve detecting
       of lack of RTP activity

    Category: Resources/res_pjsip_session

   ASTERISK-29655: res_pjsip_session: No video to caller if no camera
   available
   Reported by: Michael Auracher
     * [1e6991f95e] Maximilian Fridrich -- core_unreal: Flip stream direction
       of second channel.
     * [37829b4461] Maximilian Fridrich -- app_dial: Flip stream direction of
       outgoing channel.
   ASTERISK-29638: res_pjsip_session: No video after early media
   Reported by: Michael Auracher
     * [1e6991f95e] Maximilian Fridrich -- core_unreal: Flip stream direction
       of second channel.
     * [37829b4461] Maximilian Fridrich -- app_dial: Flip stream direction of
       outgoing channel.

    Category: Resources/res_stir_shaken

   ASTERISK-30024: Failed to sign STIR/SHAKEN payload with functionality not
   enabled
   Reported by: Claude Diderich
     * [40f4268f2d] Ben Ford -- res_pjsip_stir_shaken.c: Fix enabled when not
       configured.

    Category: pjproject/pjsip

   ASTERISK-30015: pjsip / WebRTC: Chrome creating large number of SDP
   attributes
   Reported by: Josh Hogan
     * [8500210611] Joshua C. Colp -- pjsip: Increase maximum number of
       format attributes.
   ASTERISK-29535: Segmentation fault in libasteriskpj.so.2
   Reported by: Daniel Bonazzi
     * [ec5b449bcf] Kevin Harwell -- res_pjsip_header_funcs: wrong pool used
       tdata headers

  Improvement

    Category: Applications/General

   ASTERISK-29951: app_mf, app_sf: Return -1 on hangup
   Reported by: N A
     * [9024bb989b] Naveen Albert -- app_mf, app_sf: Return -1 if channel
       hangs up.

    Category: Applications/app_dial

   ASTERISK-25716: Documentation: Document explanations and examples for
   possible values of DIALSTATUS
   Reported by: Rusty Newton
     * [a66b6647b2] Naveen Albert -- app_dial: Document DIALSTATUS return
       values.

    Category: Applications/app_meetme

   ASTERISK-29954: app_meetme: Emit warning if conference not found
   Reported by: N A
     * [b2d5bd4cb8] Naveen Albert -- app_meetme: Emit warning if conference
       not found.

    Category: Codecs/codec_opus

   ASTERISK-29980: build: External binary modules don't use https
   Reported by: INVADE International Ltd.
     * [2b636f3766] Sean Bright -- download_externals: Use HTTPS for
       downloads

    Category: Core/BuildSystem

   ASTERISK-29970: Use pkg-config to find libxml2 headers and libraries
   Reported by: Hugh McMaster
     * [b678624f04] Hugh McMaster -- configure.ac: Use pkg-config to detect
       libxml2
   ASTERISK-29980: build: External binary modules don't use https
   Reported by: INVADE International Ltd.
     * [2b636f3766] Sean Bright -- download_externals: Use HTTPS for
       downloads

    Category: Documentation

   ASTERISK-24827: Missing documentation for chan_dahdi dial string ring
   cadences
   Reported by: Scott Griepentrog
     * [085f33b7a3] Naveen Albert -- chan_dahdi: Document dial resource
       options.
   ASTERISK-29940: general: Add since tags to xmldocs
   Reported by: N A
     * [4aac359d79] Naveen Albert -- documentation: Adds versioning
       information.
   ASTERISK-29976: Should Readme include information about install_prereq
   script?
   Reported by: Marcel Wagner
     * [a893fdd901] Marcel Wagner -- documentation: Add information on
       running install_prereq script in readme
   ASTERISK-25716: Documentation: Document explanations and examples for
   possible values of DIALSTATUS
   Reported by: Rusty Newton
     * [a66b6647b2] Naveen Albert -- app_dial: Document DIALSTATUS return
       values.

    Category: PBX/General

   ASTERISK-29967: pbx_builtins: Add missing documentation
   Reported by: N A
     * [b407511f02] Naveen Albert -- pbx_builtins: Add missing options
       documentation

    Category: Resources/NewFeature

   ASTERISK-29726: Add Asterisk External Application Protocol (AEAP)
   implementation
   Reported by: Kevin Harwell
     * [2fb8667908] Kevin Harwell -- res_aeap & res_speech_aeap: Add Asterisk
       External Application Protocol

    Category: Resources/res_pjsip

   ASTERISK-29351: Qualify pjproject 2.12 for Asterisk
   Reported by: George Joseph
     * [e5e02f783d] Joshua C. Colp -- pjproject: Update bundled to 2.12
       release.

    Category: Resources/res_speech/NewFeature

   ASTERISK-29726: Add Asterisk External Application Protocol (AEAP)
   implementation
   Reported by: Kevin Harwell
     * [2fb8667908] Kevin Harwell -- res_aeap & res_speech_aeap: Add Asterisk
       External Application Protocol

     ----------------------------------------------------------------------

                      Commits Not Associated with an Issue

                                 [Back to Top]

   This is a list of all changes that went into this release that did not
   reference a JIRA issue.

   +------------------------------------------------------------------------+
   | Revision   | Author               | Summary                            |
   |------------+----------------------+------------------------------------|
   | 4c4d11eb6c | Asterisk Development | Update for 18.12.0-rc1             |
   |            | Team                 |                                    |
   |------------+----------------------+------------------------------------|
   | efca7f4e8d | Asterisk Development | Update CHANGES and UPGRADE.txt for |
   |            | Team                 | 18.12.0                            |
   |------------+----------------------+------------------------------------|
   | 62f8e157fb | Ben Ford             | res_aeap: Add basic config         |
   |            |                      | skeleton and CLI commands.         |
   |------------+----------------------+------------------------------------|
   | 801317ae05 | Asterisk Development | Update CHANGES and UPGRADE.txt for |
   |            | Team                 | 18.11.2                            |
   |------------+----------------------+------------------------------------|
   | f325cb3d13 | Asterisk Development | Update CHANGES and UPGRADE.txt for |
   |            | Team                 | 18.11.0                            |
   |------------+----------------------+------------------------------------|
   | 777e9fde67 | Sean Bright          | openssl: Supress deprecation       |
   |            |                      | warnings from OpenSSL 3.0          |
   +------------------------------------------------------------------------+

     ----------------------------------------------------------------------

                                Diffstat Results

                                 [Back to Top]

   This is a summary of the changes to the source code that went into this
   release that was generated using the diffstat utility.

 asterisk-18.11.0-summary.html                                                                 |  147
 asterisk-18.11.0-summary.txt                                                                  |  381
 b/.version                                                                                    |    2
 b/CHANGES                                                                                     |   76
 b/ChangeLog                                                                                   |  910
 b/Makefile                                                                                    |    9
 b/README.md                                                                                   |    5
 b/UPGRADE.txt                                                                                 |   12
 b/apps/app_confbridge.c                                                                       |   22
 b/apps/app_dial.c                                                                             |   55
 b/apps/app_dtmfstore.c                                                                        |    5
 b/apps/app_meetme.c                                                                           |   18
 b/apps/app_mf.c                                                                               |   29
 b/apps/app_queue.c                                                                            |   38
 b/apps/app_sendtext.c                                                                         |    5
 b/apps/app_sf.c                                                                               |   39
 b/apps/app_waitforcond.c                                                                      |    5
 b/apps/confbridge/conf_config_parser.c                                                        |    7
 b/apps/confbridge/include/confbridge.h                                                        |    1
 b/asterisk-18.12.0-rc1-summary.html                                                           |  301
 b/asterisk-18.12.0-rc1-summary.txt                                                            |  715
 b/bridges/bridge_simple.c                                                                     |   21
 b/build_tools/download_externals                                                              |    2
 b/build_tools/make_xml_documentation                                                          |   42
 b/cdr/cdr_adaptive_odbc.c                                                                     |    1
 b/channels/chan_dahdi.c                                                                       |   59
 b/channels/chan_iax2.c                                                                        |   34
 b/channels/chan_pjsip.c                                                                       |   38
 b/channels/chan_sip.c                                                                         |   14
 b/channels/pjsip/dialplan_functions.c                                                         |   16
 b/codecs/codecs.xml                                                                           |   10
 b/configs/samples/aeap.conf.sample                                                            |   21
 b/configs/samples/chan_dahdi.conf.sample                                                      |    5
 b/configs/samples/confbridge.conf.sample                                                      |    6
 b/configs/samples/func_odbc.conf.sample                                                       |    4
 b/configs/samples/pjsip.conf.sample                                                           |   12
 b/configs/samples/queues.conf.sample                                                          |    3
 b/configs/samples/stir_shaken.conf.sample                                                     |   18
 b/configure                                                                                   |18310 +++++-----
 b/configure.ac                                                                                |    1
 b/contrib/ast-db-manage/config/versions/0bee61aa9425_allow_180_ringing_with_sdp.py            |   36
 b/contrib/realtime/mysql/mysql_config.sql                                                     |    6
 b/contrib/realtime/postgresql/postgresql_config.sql                                           |    6
 b/doc/asterisk.8                                                                              |    4
 b/funcs/func_channel.c                                                                        |    5
 b/funcs/func_db.c                                                                             |   72
 b/funcs/func_env.c                                                                            |   10
 b/funcs/func_evalexten.c                                                                      |  147
 b/funcs/func_frame_drop.c                                                                     |    5
 b/funcs/func_json.c                                                                           |    5
 b/funcs/func_math.c                                                                           |   15
 b/funcs/func_odbc.c                                                                           |   39
 b/funcs/func_sayfiles.c                                                                       |    5
 b/funcs/func_scramble.c                                                                       |    5
 b/funcs/func_strings.c                                                                        |    5
 b/funcs/func_talkdetect.c                                                                     |    3
 b/include/asterisk/config.h                                                                   |   13
 b/include/asterisk/pbx.h                                                                      |   19
 b/include/asterisk/res_aeap.h                                                                 |  370
 b/include/asterisk/res_aeap_message.h                                                         |  374
 b/include/asterisk/res_pjsip.h                                                                |    9
 b/include/asterisk/res_stir_shaken.h                                                          |   54
 b/include/asterisk/speech.h                                                                   |    6
 b/include/asterisk/time.h                                                                     |   20
 b/main/Makefile                                                                               |    1
 b/main/asterisk.c                                                                             |   53
 b/main/conversions.c                                                                          |    4
 b/main/core_unreal.c                                                                          |   31
 b/main/file.c                                                                                 |    6
 b/main/logger.c                                                                               |   15
 b/main/manager.c                                                                              |    8
 b/main/pbx.c                                                                                  |   79
 b/main/pbx_builtins.c                                                                         |    7
 b/main/pbx_variables.c                                                                        |   54
 b/main/time.c                                                                                 |   29
 b/menuselect/configure                                                                        | 3730 +-
 b/menuselect/configure.ac                                                                     |    2
 b/menuselect/menuselect.c                                                                     |   70
 b/res/Makefile                                                                                |    1
 b/res/res.xml                                                                                 |    2
 b/res/res_aeap.c                                                                              |  404
 b/res/res_aeap.exports.in                                                                     |    7
 b/res/res_aeap/aeap.c                                                                         |  501
 b/res/res_aeap/general.c                                                                      |   58
 b/res/res_aeap/general.h                                                                      |   41
 b/res/res_aeap/logger.h                                                                       |   60
 b/res/res_aeap/message.c                                                                      |  270
 b/res/res_aeap/message_json.c                                                                 |  191
 b/res/res_aeap/transaction.c                                                                  |  284
 b/res/res_aeap/transaction.h                                                                  |  123
 b/res/res_aeap/transport.c                                                                    |  156
 b/res/res_aeap/transport.h                                                                    |  209
 b/res/res_aeap/transport_websocket.c                                                          |  249
 b/res/res_aeap/transport_websocket.h                                                          |   34
 b/res/res_agi.c                                                                               |   10
 b/res/res_calendar_caldav.c                                                                   |    4
 b/res/res_calendar_icalendar.c                                                                |    4
 b/res/res_crypto.c                                                                            |    2
 b/res/res_http_media_cache.c                                                                  |    7
 b/res/res_odbc.c                                                                              |    4
 b/res/res_pjsip/config_global.c                                                               |   21
 b/res/res_pjsip/config_transport.c                                                            |    6
 b/res/res_pjsip/location.c                                                                    |    5
 b/res/res_pjsip/pjsip_config.xml                                                              |   20
 b/res/res_pjsip/pjsip_configuration.c                                                         |    1
 b/res/res_pjsip/pjsip_options.c                                                               |    4
 b/res/res_pjsip_header_funcs.c                                                                |    8
 b/res/res_pjsip_history.c                                                                     |   25
 b/res/res_pjsip_pubsub.c                                                                      |   19
 b/res/res_pjsip_registrar.c                                                                   |    5
 b/res/res_pjsip_sdp_rtp.c                                                                     |   48
 b/res/res_pjsip_stir_shaken.c                                                                 |   24
 b/res/res_rtp_asterisk.c                                                                      |    1
 b/res/res_speech.c                                                                            |   36
 b/res/res_speech_aeap.c                                                                       |  731
 b/res/res_stir_shaken.c                                                                       |   96
 b/res/res_stir_shaken/curl.c                                                                  |  177
 b/res/res_stir_shaken/curl.h                                                                  |    5
 b/res/res_stir_shaken/profile.c                                                               |  241
 b/res/res_stir_shaken/profile.h                                                               |   39
 b/res/res_stir_shaken/profile_private.h                                                       |   40
 b/res/res_stir_shaken/stir_shaken.c                                                           |   29
 b/res/res_stir_shaken/stir_shaken.h                                                           |    7
 b/res/res_tonedetect.c                                                                        |   15
 b/res/stasis_recording/stored.c                                                               |    6
 b/tests/test_aeap.c                                                                           |  252
 b/tests/test_aeap_speech.c                                                                    |  287
 b/tests/test_aeap_transaction.c                                                               |  179
 b/tests/test_aeap_transport.c                                                                 |  249
 b/tests/test_conversions.c                                                                    |   12
 b/third-party/pjproject/patches/0000-configure-ssl-library-path.patch                         |   29
 b/third-party/pjproject/patches/0000-remove-third-party.patch                                 |   33
 b/third-party/pjproject/patches/0100-allow_multiple_auth_headers.patch                        |  413
 build_tools/get_sourceable_makeopts                                                           |   54
 third-party/pjproject/patches/0000-set_apps_initial_log_level.patch                           |   53
 third-party/pjproject/patches/0000-solaris.patch                                              |  135
 third-party/pjproject/patches/0011-sip_inv_patch.patch                                        |   39
 third-party/pjproject/patches/0020-pjlib_cancel_timer_0.patch                                 |   39
 third-party/pjproject/patches/0050-fix-race-parallel-build.patch                              |   72
 third-party/pjproject/patches/0060-clone-sdp-for-sip-timer-refresh-invite.patch               |   28
 third-party/pjproject/patches/0070-fix-incorrect-copying-when-creating-cancel.patch           |   37
 third-party/pjproject/patches/0080-fix-sdp-neg-modify-local-offer.patch                       |   33
 third-party/pjproject/patches/0090-Skip-unsupported-digest-algorithm-2408.patch               |  212
 third-party/pjproject/patches/0100-fix-double-stun-free.patch                                 |   82
 third-party/pjproject/patches/0110-tls-parent-listener-destroyed.patch                        |  166
 third-party/pjproject/patches/0111-ssl-premature-destroy.patch                                |  136
 third-party/pjproject/patches/0120-pjmedia_sdp_attr_get_rtpmap-Strip-param-trailing-whi.patch |   32
 third-party/pjproject/patches/0130-sip_inv-Additional-multipart-support-2919-2920.patch       |  661
 third-party/pjproject/patches/0140-Fix-incorrect-unescaping-of-tokens-during-parsing-29.patch |  123
 third-party/pjproject/patches/0150-Create-generic-pjsip_hdr_find-functions.patch              |  176
 third-party/pjproject/patches/0160-Additional-multipart-improvements.patch                    |  644
 third-party/pjproject/patches/0170-stun-integer-underflow.patch                               |   26
 third-party/pjproject/patches/0171-dialog-set-free.patch                                      |  114
 third-party/pjproject/patches/0172-prevent-multipart-oob.patch                                |   22
 154 files changed, 21655 insertions(+), 13634 deletions(-)
